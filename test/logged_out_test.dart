import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:woky/screens/loggedOut.dart';
import 'package:woky/screens/login.dart';
import 'package:woky/screens/signUp.dart';
import 'package:woky/widgets/loggedOut/loggedOutBody.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  Widget testWidget({Widget child}) {
    return MaterialApp(home: child);
  }

  group('UI Generation tests', () {
    testWidgets('Text in logged out', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget(child: LoggedOut()));

      // Test gradient of background color
      expect(
          ((tester.firstWidget(find.byType(Container)) as Container).decoration
                  as BoxDecoration)
              .gradient,
          LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color(0xFFE11C34), Color(0xFF500B28)]));

      // Test logo woky visible
      expect(find.text('woky'), findsOneWidget);
    });
  });

  group('Navigation tests', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    Future<Null> _buildLoggedOutPage(WidgetTester tester) async {
      await tester.pumpWidget(MaterialApp(
        home: LoggedOutBody(),

        /// This mocked observer will now receive all navigation events
        /// that happen in our app.
        navigatorObservers: [mockObserver],
      ));

      verify(mockObserver.didPush(any, any));
    }

    Future<Null> _navigateToSignUpPage(WidgetTester tester) async {
      await tester.tap(find.byKey(LoggedOutBody.navigateToSignUpButtonKey));
      await tester.pumpAndSettle();
    }

    Future<Null> _navigateToLogInPage(WidgetTester tester) async {
      await tester.tap(find.byKey(LoggedOutBody.navigateToLogInButtonKey));
      await tester.pumpAndSettle();
    }

    testWidgets(
        'when tapping "Register" button, should navigate to sign up page',
        (WidgetTester tester) async {
      await _buildLoggedOutPage(tester);
      await _navigateToSignUpPage(tester);

      verify(mockObserver.didPush(any, any));

      expect(find.byType(SignUp), findsOneWidget);

      expect(find.text('Sign Up'), findsOneWidget);
    });
    testWidgets('when tapping "Login" button, should navigate to sign up page',
        (WidgetTester tester) async {
      await _buildLoggedOutPage(tester);
      await _navigateToLogInPage(tester);

      verify(mockObserver.didPush(any, any));

      expect(find.byType(Login), findsOneWidget);

      expect(find.text('Log in'), findsOneWidget);
    });
  });
}
