import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:woky/screens/signUp.dart';

void main() {
  Widget testWidget({Widget child}) {
    return MaterialApp(home: child);
  }

  testWidgets('Sign Up UI', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(testWidget(child: SignUp()));

    expect(find.text('Sign Up'), findsOneWidget);
  });
}
