import 'package:flutter/material.dart';
import 'package:woky/widgets/loggedOut/loggedOutBody.dart';

class LoggedOut extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoggedOutBody(),
    );
  }
}
